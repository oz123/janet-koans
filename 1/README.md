Getting started with Janet
==========================

You can start janet in your shell:
```
$ janet
Janet 1.9.0-dev-73dba69  Copyright (C) 2017-2020 Calvin Rose
janet:1:>
```

The REPL is similar to Python's or other REPLs:

```
(print "Hello World!")
Hello World!
nil
janet:2:> 
```

It's a LISP! Everything is wrapped in braces. Like it or not ...

* Things have a explicit return value! Even if it's a simple print.

You can run janet programs (could be anything but for convinience mark
files with the ending ".janet" or ".j"):

```
$ janet hello.j
Hello World
```
