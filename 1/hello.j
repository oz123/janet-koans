# define a function which takes no parameters and retruns
# Hello World
(defn hello [] "Hello World!")

# janet is a LISP, function calls are written in Polish notations
(assert (= (hello) "Hello World!"))

(print "Hello World")
