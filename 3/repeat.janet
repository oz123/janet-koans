(defn repeat "repeat a character 'c' n times" [c n]
  (var str "")  # defines an immutable variable
  (var i 0)
  (while (< i n)
   (++ i)
   (set str (string c str))  # concatenate c with str and update str
  )
  str  # a function evaluated to it's last statement
)


(assert (= "ccccc" (repeat "c" 5)))

# more loops
# (loop [i :down-to [10 1]] (print i))


(defn repeat2 "repeat using 'loop'" [c n]
  (var str "")
  (loop [i :range [0 5]] (set str (string c str)))
  str
)

(assert (= "ccccc" (repeat2 "c" 5)))
