(assert (= 3 (+ 1 2))) # works
# (assert (= 3 (+ 3 2))) # fails

# define a function that adds two numbers
(defn Add "Add two numbers" [x y] (+ x y))
```
You can show the documentation of an element
with:

janet:4:> (doc Add)


    function
    repl on line 1, column 1

    (Add x y)

    Add two numbers


nil
```
(assert (= 5 (Add 3 2)))
